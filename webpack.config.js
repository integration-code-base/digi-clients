const path = require('path');
const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');

module.exports = {
  mode: 'development',
  entry: './app/simaland/digiCustom/common.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'app/simaland/digiCustom/dist'),
  },
  plugins: [
    new MergeIntoSingleFilePlugin({
      files: {
        'bundle.js': [
          path.resolve(__dirname, 'app/simaland/digiCustom/common.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/links.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/digiViewEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/digiClickEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/clientsViewEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/clientsClickEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/clientsTrackingEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/digiTrackingEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/clientsThrowEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/zeroResultsEvents.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/handlers.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/actions.js'),
          path.resolve(__dirname, 'app/simaland/digiCustom/modules/hooks.js'),
        ],
      },
    }),
  ],
};