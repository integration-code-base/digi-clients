window.digiCustom = {
  isMobile: function () {
    var isMobile = false;
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigator.userAgent
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        navigator.userAgent.substr(0, 4)
      ) ||
      ((/iPad|iPhone|iPod/.test(navigator.platform) ||
        (navigator.platform === "MacIntel" && navigator.maxTouchPoints > 1)) &&
        !window.MSStream)
    )
      isMobile = true;
    return isMobile;
  },
  addCustomScrollEvent: function () {
    /* Вызываем через хук onContainerOpen */
    document.querySelector(".digi-main-scroll-wrapper").addEventListener("scroll", window.digiCustom.customScrollEvent);
  },
  customScrollEvent: function () {
    var digiScrollWrapper = document.querySelector(".digi-main-scroll-wrapper");
    var digiShield = document.querySelector("#digi-shield");
    var headerMain = document.querySelector('.new-header-sub .header-main')
    var body = document.querySelector('body');
    var currentScroll = window.pageYOffset;
    var newHeaderSub = document.querySelector(".new-header-sub").offsetTop;

    /* currentScroll в данном случае это скролл страницы, здесь мы проверяем на то,
     что закроленную шапку не нужно отрисовывать, если она уже есть */
    if (digiScrollWrapper.scrollTop > newHeaderSub && currentScroll < 210) {
      scrollingDown()
    }
    if (digiScrollWrapper.scrollTop < newHeaderSub && currentScroll < 210) {
      scrollingBack()
    }

    function scrollingDown() {
      digiShield.style.top = newHeaderSub + "px";
      digiShield.style.height = "calc(100vh - " + newHeaderSub + "px)";
      /* Стиль клиента при котором меняется шапка */
      body.classList.add('header-fixed');
      headerMain.classList.add('digi-z-decrease');
    }

    function scrollingBack() {
      body.classList.remove('header-fixed')
      headerMain.classList.remove('digi-z-decrease');
      var headerHeight = document.querySelector(".header").offsetHeight;
      digiShield.style.top = headerHeight + "px";
      digiShield.style.height = "calc(100vh - " + headerHeight + "px)";
    }
  },
  afterWidgetClose: function () {
    /* Вызываем через хук onContainerClose, возвращаем в исходное состояние страницу */
    var headerMain = document.querySelector('.new-header-sub .header-main');
    var currentScroll = window.pageYOffset;
    var body = document.querySelector('body');

    if (currentScroll < 210) {
      body.classList.remove('header-fixed')
    }

    if (headerMain.classList.contains('digi-z-decrease')) {
      headerMain.classList.remove('digi-z-decrease')
    }
  },
  currentRegion: document.querySelector('#check_location').innerText.replace(/,+/, ''),
  getRegionAvailable: function () {
    /* Может выглядить очень странно, но это была не совсем правильная рекомендация по неймингу,
     у клиента два региона и остатки передаются в атрибуты по такой логике*/
    if(digiCustom.currentRegion === 'Москва') {
      return 'available'
    } else {
      return 'наличие товара'
    }
  },
  addToFavorite: function (e, id) {
    if (e.target.classList.contains("digi-addTo-favorite-icon-active")) {
      e.target.classList.remove("digi-addTo-favorite-icon-active");
      window.addToFavorite(id, "remove");
    } else {
      e.target.classList.add("digi-addTo-favorite-icon-active");
      window.addToFavorite(id, "add");
    }
  },
  notAvailableRenderedElements: {},
  renderNotAvailable(commonParent, available, id) {
    /* Подсказка о недоступности  https://i.imgur.com/6iGW6iy.png
      проверяем на то, что элемента с данным id еще нет в объекте, иначе они отрисовываются поверх*/
    if(!window.digiCustom.notAvailableRenderedElements[id]) {
      var notAvailable = document.createElement('div');
      window.digiCustom.notAvailableRenderedElements[id] = 'rendered';
      notAvailable.classList.add('digi-notice-not-available');
      notAvailable.innerHTML = 'К сожалению, данного товара на складе осталось <br> только ' + available + ' шт.';
      commonParent.querySelector('.digi__quantity-block').append(notAvailable);

      /* Через 5 сек удаляем элемнт со страницы и удаляем поле из объекта */
      setTimeout(function () {
        if(notAvailable) {
          delete window.digiCustom.notAvailableRenderedElements[id]
          notAvailable.remove()
        }
      }, 5000)
    }
  },
  changeAddToCartInputValue(e, id, available) {
    var quantityInput = e.target;
    var commonParent = window.digiCustom.getCommonParent(e.target, 'digi-product__actions');

    /* Ничего не делаем если введено пустое или нулевое значения и товара нет в нашем объекте */
    if(!window.digiCustom.cartItemsSimple[id] && (+quantityInput.value === 0 || quantityInput.value === '')) return

    /* Товарн не был добавлен, меняем отображение */
    if (!window.digiCustom.cartItemsSimple[id] && quantityInput.value >= 1) {
      window.digiCustom.changeButtonsDisplay(commonParent);
    }

    window.digiCustom.changeActionText(commonParent, 'change')

    /* Если добавлено больше чем доступно */
    if (+available < +quantityInput.value) {
      quantityInput.value = +available
      window.digiCustom.renderNotAvailable(commonParent, available, id)
    }

    /* Если товар был в корзине и значения инпута введено 0 или пустая строка
     мы оставляем в инпуте 1, такая вот логика клиента */
    if(window.digiCustom.cartItemsSimple[id] && (+quantityInput.value === 0 || quantityInput.value === '')) {
      quantityInput.value = '1'
    }

    /* После всех операция присваиваем конечно значение */
    window.digiCustom.cartItemsSimple[id] = quantityInput.value;

    /* Промис после которого обновляем состояние корзины и дергаем клиентскую логику,
     которая делает визуальное обновление корзины */
    window.digiCustom.basketUpdate(id, quantityInput.value).then(function () {
      window.digiCustom.debauncedRefreshCartItems();

      window.jQuery('.qcart_new').load(window.location.href + ' .qcart_new', function() {
        countItemsCart();
        blazy.revalidate();
      });
    })
  },
  basketUpdate(id, quantity) {
    /* Чисто запрос к серверу для обновление корзины в который передаем id и количество товара */
    return new Promise((resolve, reject) => {
      var XHR = new XMLHttpRequest();
      var url = '/ajax/basket_ubdate.php';

      var body = 'id=' + id + '&quantity=' + quantity;
      XHR.open('POST', url);

      XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

      XHR.onload = () => {
        if (XHR.status >= 400) {
          reject(XHR.response)
        } else {
          resolve(XHR.response)
        }
      }

      XHR.onerror = () => {
        reject(XHR.response)
      }

      XHR.send(body);
    })
  },
  addToCart: function (e, id, available) {
    var commonParent = window.digiCustom.getCommonParent(e.target, 'digi-product__actions');
    var quantityInput = commonParent.querySelector(".digi__quantity-input");

    if (+available > +quantityInput.value) {
      if (!window.digiCustom.cartItemsSimple[id]) {
        window.digiCustom.changeButtonsDisplay(commonParent);
        window.digiCustom.cartItemsSimple[id] = 1;
      } else {
        window.digiCustom.cartItemsSimple[id]++
      }

      quantityInput.value++;
      /* Клиентская функция добавления в корзину */
      window.addToCart(id, "add");

      window.digiCustom.debauncedRefreshCartItems();
      window.digiCustom.changeActionText(commonParent, 'add')
    } else {
      window.digiCustom.renderNotAvailable(commonParent, available, id)
    }
  },
  removeToCart: function (e, id) {
    var commonParent = window.digiCustom.getCommonParent(e.target, 'digi-product__actions');
    var quantityInput = commonParent.querySelector(".digi__quantity-input");

    /* Особенность, если товар был добавлне то мы меняем значение с одного на ноль, в свою очередь через изменение
    * инпута нельзя передавать значения меньше 0. Они постоянно меняли эту логику в процессе интеграции.  */
    if (quantityInput.value === '1' && window.digiCustom.cartItemsSimple[id]) {
      quantityInput.value--;
      delete window.digiCustom.cartItemsSimple[id]
      window.digiCustom.changeButtonsDisplay(commonParent, 'deleting')
    } else if (quantityInput.value > 1) {
      quantityInput.value--;
      window.digiCustom.cartItemsSimple[id]--
      window.digiCustom.changeActionText(commonParent, 'change')
    }

    /* Клиентская функция удаление из корзины */
    window.addToCart(id, "remove");
    window.digiCustom.debauncedRefreshCartItems();
  },
  changeButtonsDisplay(commonParent, action = 'adding') {
    /* Изменение отображение кнопок добавление в корзину для добавленых и удаленных товаров https://i.imgur.com/LHQLbZD.png */
    var buttonWrapperEmpty = commonParent.querySelector('.digi-add-btn-wrapper--empty');
    var buttonWrapperAdded = commonParent.querySelector('.digi-add-btn-wrapper--added');

    if (action === 'adding' && buttonWrapperAdded.classList.contains('digi-add-btn-wrapper--hidden')) {
      buttonWrapperEmpty.classList.add("digi-add-btn-wrapper--hidden");
      buttonWrapperAdded.classList.remove("digi-add-btn-wrapper--hidden");
    } else if (action === 'deleting' && buttonWrapperEmpty.classList.contains('digi-add-btn-wrapper--hidden')) {
      buttonWrapperEmpty.classList.remove("digi-add-btn-wrapper--hidden");
      buttonWrapperAdded.classList.add("digi-add-btn-wrapper--hidden");
    }
  },
  changeActionText(commonParent, action = 'add') {
    /* Меняем текст в зависемости от действия,
    удаление или изменение инпута = change, все остальное по дефолтку add */
    var actionTextElement = commonParent.querySelector('.digi-add-text');
    if(actionTextElement) {
      if(action === 'add' && actionTextElement.classList.contains('digi-add-text--changed')) {
        actionTextElement.innerHTML = 'Добавлено в <a href="/personal/cart">корзину</a>'
        actionTextElement.classList.remove('digi-add-text--changed');
        actionTextElement.classList.add('digi-add-text--added');
      } else if (action === 'change' && actionTextElement.classList.contains('digi-add-text--added')) {
        actionTextElement.innerHTML = 'Изменено в <a href="/personal/cart">корзине</a>'
        actionTextElement.classList.remove('digi-add-text--added');
        actionTextElement.classList.add('digi-add-text--changed');
      }
    }
  },
  getCommonParent: function (elem, parentClass) {
    /* Полифил фунции closest, который на самом деле не имеет здесь большого
    * смысла после использование промисов :) */
    var currentParrent = elem.parentElement;
    while (!currentParrent.classList.contains(parentClass)) {
      currentParrent = currentParrent.parentElement;
    }
    return currentParrent;
  },
  checkInSimpleCartItems: function (id) {
    /* Вроде, нигде не используем, но на всякий случай оставлю :) */
    var cartItemsSimple = window.digiCustom.cartItemsSimple;
    if (!cartItemsSimple[id]) {
      return '';
    }
    return cartItemsSimple[id];
  },
  refreshCartItems: function () {
    window.digiCustom.cartItemsDetailed = window.arrItemsCart();

    /* Из за того, что функия клиента возвращает состояние корзины
     асинхронно добавлен небольшой таймаут передтем как обновлять наш объект */
    setTimeout(function () {
      window.digiCustom.cartItemsSimple = window.digiCustom.collectCartItemsSimple(window.digiCustom.cartItemsDetailed);
    }, 900);
  },

  debounce: function (fn, interval) {
    let timer;

    return function debounced() {
      clearTimeout(timer);
      let args = arguments;
      let that = this;
      timer = setTimeout(function callOriginalFn() {
        fn.apply(that, args);
      }, interval);
    };
  },
  cartItemsSimple: {},
  collectCartItemsSimple: function (obj) {
    var newObj = {};
    var cartItemsDetailed = obj;
    for (var item in cartItemsDetailed) {
      newObj[cartItemsDetailed[item]["id"]] =
        cartItemsDetailed[item]["quantity"] + '';
    }
    return newObj;
  },
  setCartItemsSimple() {
    /* При загрузки собираем наш объект из объекта, который доступен уже при загрузке
    * подробнее об этом было описанно в общем описание по клиенту*/
    window.digiCustom.cartItemsSimple = window.digiCustom.collectCartItemsSimple(window.arr);
  },
};

window.digiCustom.setCartItemsSimple();

/* Из за того, что их сервер перестает отвечать после того, как происходит слишком много запросов за одну секкунду мы добавили
* данную обертку, но на своей стороне у них этой обертки нет, но когда я это писал я не знал об этом, в итоге наше решение лучше :shrug: */
window.digiCustom.debauncedRefreshCartItems = window.digiCustom.debounce(
  window.digiCustom.refreshCartItems,
  1000
);

if (!window.digiCustom.isMobile()) {
  Config.anyQuery.instantSearch.closeByClickOutside = true;
}

