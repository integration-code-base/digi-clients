{
  "silent": true,
  "hooks": {
  "onBeforeAppCreate": "(function() {})",
    "onAppCreate": "(function() {window.addEventListener('resize', function () { App.methods.setCorrectPosition.bind({ $el: document.querySelector('#digi-shield') })(App.methods.getCoordinates.bind(App.data())())})})",
    "onContainerOpen": "(function() { if(!this.state.isMobile){window.digiCustom.addCustomScrollEvent()}; if (this.state.isMobile) { setTimeout(function () { document.querySelector('.digi-search-form__input-block input').focus()}, 100)};})",
    "onContainerClose": "(function() {if(!this.state.isMobile){window.digiCustom.afterWidgetClose()}})",
    "onSearchFieldFocus": "(function() { window.digiCustom.setCartItemsSimple(); this.state.isLoading = true; this.state.isLoading = false })",
    "onSearchFieldInput": "(function() {})",
    "onBeforeSearch": "(function() {})",
    "onSearch": "(function() {})",
    "onSearchButtonClick": "(function() {})",
    "onSearchFormSubmit": "(function() {})",
    "onRefinementClick": "(function() {})",
    "onAutocompleteClickTap": "(function(payload) {})",
    "onAutocompleteClickQuery": "(function(payload) {})",
    "onAutocompleteClickHistory": "(function(payload) {})",
    "onAutocompleteClickCategory": "(function(payload) {})",
    "onAutocompleteClickProduct": "(function(payload) {})",
    "onFacetChange": "(function(payload) {})",
    "onCategoryClick": "(function(payload) {})"
}
}