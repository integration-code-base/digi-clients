window.digiCustom.throwClientsMobileViewEvent = function (event) {
  var autocompleteListsElements = document.querySelectorAll('.tips-list__list');
  var autocompleteListTitleElements = document.querySelectorAll('.tips-list__title');

  for (var i = 0; i < autocompleteListsElements.length; i++) {
    var listTitle = autocompleteListTitleElements[i];

    if (listTitle.innerText.trim() === 'Популярные запросы') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewQueryEventData(
              autocompleteListsElements[i].querySelectorAll('.search-tip__wrapper'),
              event.target.value,
          ),
      );
    } else if (listTitle.innerText.trim() === 'Категории') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewCategoriesEventData(
              autocompleteListsElements[i].querySelectorAll('.search-tip__wrapper'),
              event.target.value,
          ),
      );
    } else if (listTitle.innerText.trim() === 'Товары') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewProductEventData(
              autocompleteListsElements[i].querySelectorAll('.search-tip__wrapper'),
              event.target.value,
              'mobile',
          ),
      );
    }
  }
};

window.digiCustom.throwClientsDesktopViewEvent = function (event) {
  var autocompleteLists = document.querySelectorAll('.search-tips__list .container, .tips-list__container');

  if(event.target.value === '') return;

  for (let i = 0; i < autocompleteLists.length; i++) {
    var listTitle = autocompleteLists[i].querySelector(
        '.tips-list__title, .items-tips__title',
    );

    if (listTitle.innerText.trim() === 'Часто ищут') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewQueryEventData(
              autocompleteLists[i].querySelectorAll('.tips-list__tip'),
              event.target.value,
          ),
      );
    } else if (listTitle.innerText.trim() === 'Категории') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewCategoriesEventData(
              autocompleteLists[i].querySelectorAll('.tips-list__tip'),
              event.target.value,
          ),
      );
    } else if (listTitle.innerText.trim() === 'Товары') {
      window.dataLayer.push(
          window.digiCustom.generateClientsViewProductEventData(
              autocompleteLists[i].querySelectorAll('.items-tips__tip'),
              event.target.value,
              'desktop',
          ),
      );
    }
  }
};

if (window.digiCustom.isClientMobile()) {
  var autocompleteOpenButton = document.querySelector('._2ttRV');

  if (autocompleteOpenButton) {
    autocompleteOpenButton.addEventListener('click', waitForInput);

    function waitForInput() {
      var timeoutId = setTimeout(function tick() {
        var autocompleteInput = document.querySelector('.search-bar__search-field');

        if (autocompleteInput) {
          autocompleteInput.addEventListener(
              'input',
              window.digiCustom.debounce(window.digiCustom.throwClientsMobileViewEvent, 2000),
          );
        } else {
          timeoutId = setTimeout(tick, 500);
        }
      }, 500);
    }
  }
} else {
  document.querySelector('.search-field__input').addEventListener(
      'input',
      window.digiCustom.debounce(window.digiCustom.throwClientsDesktopViewEvent, 2000),
  );
}
