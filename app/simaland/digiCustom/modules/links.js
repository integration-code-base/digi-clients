window.digiCustom.initData = function () {
  window.digiCustom.data = {
    digiShield: document.querySelector('#digi-shield'),
    closeByClickOutsideIgnoreSelectorList: [
      '#digi-shield',
      window.Digi.config.anyQuery.instantSearch.submitSelector,
      window.Digi.config.anyQuery.instantSearch.openButtonSelector,
      window.Digi.config.anyQuery.instantSearch.inputSelector,
      '.digi-search',
    ],
    isZeroSearch: false,
    getDigiSearchInput: function () {
      if(!window.digiCustom.data.digiSearchInputStored) {
        window.digiCustom.data.digiSearchInputStored = document.querySelector('.digi-search-form__input');
      }
      return window.digiCustom.data.digiSearchInputStored;
    }
  }
}