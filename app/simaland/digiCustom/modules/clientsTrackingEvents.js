window.digiCustom.clientsAutocompleteClickEvent = function (type, position) {
  var event = {
    eventType: 'WIDGET_CLICK',
    pageNumber: '1',
    pageProducts: null,
    position: position + 1,
    productId: '',
    strategy: '',
    widgetType: 'client_widget|' + type,
  };

  window.Digi.tracking.sendEvent(event);
};
