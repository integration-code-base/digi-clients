window.digiCustom.combineDigiProductData = function (product, index, list) {
  return {
    name: product.name,
    id: product.id,
    price: product.price,
    list,
    position: index + 1,
  };
};

window.digiCustom.pushZeroResultsViewEvent = function (term, products) {
  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryNullResultWidget',
      'ProductHintShow',
      term,
  );

  eventData.ecommerce = {
    impressions: [],
  };

  products.forEach(function (product, index) {
    eventData.ecommerce.impressions.push(
        window.digiCustom.combineDigiProductData(
            product,
            index,
            'Anyquery: Нулевые результаты',
        ),
    );
  });

  window.dataLayer.push(eventData);
};

window.digiCustom.pushZeroResultsClickEvent = function (term, product, position) {
  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryNullResultWidget',
      'ProductHintClick',
      term,
  );

  eventData.ecommerce = {
    click: {
      actionField: {'list': 'Anyquery: Нулевые результаты'},
      products: [
        window.digiCustom.combineDigiProductData(
            product,
            position,
            'Anyquery: Нулевые результаты',
        ),
      ],
    },
  };

  window.dataLayer.push(eventData);
};

