window.digiCustom.sendAutocompleteClickEvent = function (
    autocompleteItem,
    autocompleteItemType,
    productId,
    term,
    correction,
) {
  var hasCorrection = !!correction;
  var saveTerm = term || '';

  var event = {
    eventType: 'AUTOCOMPLETE_CLICK',
    anyQueryWorked: hasCorrection,
    autocompleteItem,
    autocompleteItemType,
    originalSearchTerm: saveTerm,
    productId,
    searchTerm: hasCorrection ? correction : saveTerm,
  };

  window.Digi.tracking.sendEvent(event);
};


window.digiCustom.sendSearchEvent = function (
    correction,
    term,
    refinements,
    currentPage,
    productLastIds,
) {
  const hasCorrection = !!correction;

  const event = {
    eventType: 'SEARCH_EVENT',
    anyQuerySynonyms: refinements.map(it => it.tap),
    anyQueryWorked: hasCorrection,
    originalSearchTerm: term,
    pageNumber: currentPage,
    pageProducts: productLastIds,
    searchTerm: hasCorrection ? correction : term,
  };

  window.Digi.tracking.sendEvent(event);
};

window.digiCustom.sendSearchSubmitEvent = function (
    term,
    correction,
    redirect,
    type,
    currentPage,
    autocompleteProducts,
    refinements,
    productLastIds,
) {
  var productListIds = autocompleteProducts && autocompleteProducts.map(it => it.id);

  var event = {
    eventType: 'WIDGET_CLICK',
    pageNumber: currentPage,
    pageProducts: productListIds,
    position: -1,
    productId: redirect ? `redirect|${term}` : term,
    strategy: window.Digi.config.anyQuery.autocomplete.strategy,
    widgetType: `instant_search|${type}`,
  };

  window.Digi.tracking.sendEvent(event);
  window.digiCustom.sendSearchEvent(
      correction,
      term,
      refinements,
      currentPage,
      productLastIds,
  );
};
