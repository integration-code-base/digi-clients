window.digiCustom.generateDigiViewQueryEventData = function (queries, term) {
  var promotions = [];

  queries.forEach(function (query, index) {
    var queryCopy = {};
    queryCopy.name = query.st;
    queryCopy.creative = 'Anyquery: searchHint';
    queryCopy.position = index + 1;
    promotions.push(queryCopy);
  });

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'searchHint',
      term,
  );

  eventData.ecommerce = {
    promoView: {
      promotions,
    },
  };

  return eventData;
};

window.digiCustom.generateDigiViewTapEventData = function (taps, term) {
  var promotions = [];

  taps.forEach(function (tap, index) {
    var tapCopy = {};
    tapCopy.name = tap.tap;
    tapCopy.creative = 'Anyquery: tapHint';
    tapCopy.position = index + 1;
    promotions.push(tapCopy);
  });

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'tapHint',
      term,
  );

  eventData.ecommerce = {
    promoView: {
      promotions,
    },
  };

  return eventData;
}

window.digiCustom.generateDigiViewCategoriesEventData = function (categories, term) {
  var promotions = [];

  categories.forEach(function (category, index) {
    var categoryCopy = {};
    categoryCopy.id = category.id;
    categoryCopy.name = category.name;
    categoryCopy.creative = 'Anyquery: categoryHint';
    categoryCopy.position = index + 1;

    promotions.push(categoryCopy);
  });

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'categoryHint',
      term,
  );

  eventData.ecommerce = {
    promoView: {
      promotions,
    },
  };

  return eventData;
};

window.digiCustom.generateDigiViewProductEventData = function (products, term) {
  var impressions = [];
  products.forEach(function (product, index) {
    var productCopy = {};
    productCopy.name = product.name;
    productCopy.id = product.id;
    productCopy.price = product.price;
    productCopy.list = 'Anyquery: productHint';
    productCopy.position = index + 1;
    impressions.push(productCopy);
  });

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'productsHint',
      term,
  );

  eventData.ecommerce = {
    impressions,
  };

  return eventData;
};

window.digiCustom.throwDigiViewEvent = function () {
  var autocompleteData = this.state.autocompleteData;
  var term = this.state.term;
  var queries = autocompleteData.sts.slice(0, 4);
  var categories = autocompleteData.categories.slice(0, 3);
  var productsAmount = window.digiCustom.isDigiMobile() ? 4 : 6;
  var products = autocompleteData.products.slice(0, productsAmount);
  var taps = autocompleteData.taps;

  if (term === '') return;

  window.dataLayer.push(window.digiCustom.generateDigiViewQueryEventData(queries, term));

  window.dataLayer.push(window.digiCustom.generateDigiViewCategoriesEventData(categories, term));

  window.dataLayer.push(window.digiCustom.generateDigiViewProductEventData(products, term));

  if (window.digiCustom.isDigiMobile()) {
    window.dataLayer.push(window.digiCustom.generateDigiViewTapEventData(taps, term));
  }
};

window.digiCustom.debouncedThrowDigiViewEvent = window.digiCustom.debounce(
    window.digiCustom.throwDigiViewEvent, 1000,
);
