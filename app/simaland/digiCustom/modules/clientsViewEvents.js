window.digiCustom.generateClientsViewQueryEventData = function (queryCollection, term) {
  var promotions = [];

  for (var i = 0; i < queryCollection.length; i++) {
    var queryInfo = {};
    queryInfo.name = queryCollection[i].innerText.trim();
    queryInfo.creative = 'Anyquery: searchHint';
    queryInfo.position = i + 1;
    promotions.push(queryInfo);

    queryCollection[i].addEventListener(
        'click',
        window.digiCustom.throwClientsClickQueryEvent.bind(null, queryInfo.name, i, term),
    );
    queryCollection[i].addEventListener(
        'click',
        window.digiCustom.clientsAutocompleteClickEvent.bind(null, 'query_click', i),
    );
  }

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'searchHint',
      term,
  );

  eventData.ecommerce = {
    promoView: {
      promotions,
    },
  };

  return eventData;
};

window.digiCustom.generateClientsViewCategoriesEventData = function (categoryCollection, term) {
  var promotions = [];

  for (var i = 0; i < categoryCollection.length; i++) {
    var categoryInfo = {};
    /* categoryInfo.id = category.id; // absent */
    categoryInfo.name = categoryCollection[i]
        .querySelector('.search-tip__name, .tips-list__name').innerText.trim();
    categoryInfo.creative = 'Anyquery: categoryHint';
    categoryInfo.position = i + 1;

    promotions.push(categoryInfo);

    categoryCollection[i].addEventListener(
        'click',
        window.digiCustom.throwClientsClickCategoryEvent.bind(
            null,
            categoryInfo.name,
            i,
            term
        ),
    );
    categoryCollection[i].addEventListener(
        'click',
        window.digiCustom.clientsAutocompleteClickEvent.bind(
            null,
            'category_click',
            i,
        ),
    );
  }

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'categoryHint',
      term
  );

  eventData.ecommerce = {
    promoView: {
      promotions,
    },
  };

  return eventData;
};

window.digiCustom.generateClientsViewProductEventData = function (productsCollection, term, device) {
  var impressions = [];

  for (var i = 0; i < productsCollection.length; i++) {
    var productInfo = {};

    productInfo.name = getName(productsCollection[i]);
    productInfo.price = getPrice(productsCollection[i]);
    productInfo.list = 'Anyquery: productHint';
    productInfo.position = i + 1;
    impressions.push(productInfo);
    productsCollection[i].addEventListener(
        'click',
        window.digiCustom.throwClientsClickProductEventData.bind(null, productInfo, i, term),
    );
    productsCollection[i].addEventListener(
        'click',
        window.digiCustom.clientsAutocompleteClickEvent.bind(null, 'product_click', i),
    );
  }

  function getName(product) {
    if (device === 'desktop') {
      return product.querySelector('.name').innerText.trim();
    } else {
      return product.querySelector('.search-tip__name').innerText.trim();
    }
  }

  function getPrice(product) {
    var priceElement;

    if (device === 'desktop') {
      priceElement = product.querySelector('.items-tips__price');
    } else {
      priceElement = product.querySelector('.price__price');
    }
    var splittedPrice = priceElement.innerText.split(',');

    if (splittedPrice.length > 1) {
      var whole = splittedPrice[0].replace(/\D+/g, '');
      var rest = splittedPrice[1].replace(/\D+/g, '');

      return whole + '.' + rest;
    } else {
      return splittedPrice[0].replace(/\D+/g, '');
    }
  }

  var eventData = window.digiCustom.getGAEventFrame(
      'AnyqueryWidget',
      'productsHint',
      term
  );

  eventData.ecommerce = {
    impressions,
  };

  return eventData;
};
