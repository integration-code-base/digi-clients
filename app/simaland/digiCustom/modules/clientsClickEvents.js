window.digiCustom.throwClientsClickQueryEvent = function (query, index, term) {
  var clickEvent = window.digiCustom.getPromoClickEvent(
      window.digiCustom.getGAEventFrame(
          'AnyqueryWidget',
          'searchHintClick',
          term,
      ),
      {
        'name': query,
        'creative': 'Anyquery: searchHint',
        'position': index + 1,
      },
  );

  dataLayer.push(clickEvent);
};

window.digiCustom.throwClientsClickCategoryEvent = function (categoryName, index, term) {
  var clickEvent = window.digiCustom.getPromoClickEvent(
      window.digiCustom.getGAEventFrame(
          'AnyqueryWidget',
          'categoryHintClick',
          term,
      ),
      {
        'name': categoryName,
        'creative': 'Anyquery: categoryHint',
        'position': index + 1,
      },
  );

  dataLayer.push(clickEvent);
};

window.digiCustom.throwClientsClickProductEventData = function (product, index, term) {
  var clickEvent = window.digiCustom.getClickEventContent(
      {
        'name': product.name,
        'price': product.price,
        'list': 'Anyquery: productHint',
        'position': index + 1,
      },
      term,
  );

  dataLayer.push(clickEvent);
};