window.digiCustom.handleEnter = function (
    event,
    correction,
    term,
    redirect,
    refinements,
    currentPage,
    productLastIds,
) {
  event.stopPropagation();
  event.preventDefault();

  window.Digi.utils.setHistoryItem({name: term, link_url: redirect || null, type: 'queries'});
  window.digiCustom.sendSearchEvent(
      correction,
      term,
      refinements,
      currentPage,
      productLastIds,
  );

  if (correction) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + correction;
  } else if (term) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + term;
  }
};

window.digiCustom.handleSubmitButton = function (
    term,
    correction,
    redirect,
    type,
    currentPage,
    autocompleteProducts,
    refinements,
    productLastIds,
) {
  window.Digi.utils.setHistoryItem({name: term, link_url: redirect || null, type: 'queries'});
  window.digiCustom.sendSearchSubmitEvent(
      term,
      correction,
      redirect,
      type,
      currentPage,
      autocompleteProducts,
      refinements,
      productLastIds,
  );

  if (correction) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + correction;
  } else if (term) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + term;
  }
};

window.digiCustom.handleQuery = function (query, redirectUrl, index, term, correction) {
  dataLayer.push(window.digiCustom.generateDigiClickQueryEventData(query, index, term));
  window.Digi.utils.setHistoryItem({name: query, link_url: redirectUrl || null, type: 'queries'});
  const itemType = redirectUrl ? 'redirect' : 'queries';

  window.digiCustom.sendAutocompleteClickEvent(
      query,
      itemType,
      null,
      term,
      correction,
  );

  if (redirectUrl) {
    window.location.href.href = redirectUrl;
  } else if (query) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + query;
  }
};

window.digiCustom.categoryClick = function (category, index, term, correction) {
  window.digiCustom.sendAutocompleteClickEvent(
      category,
      'categories',
      null,
      term,
      correction,
  );

  window.dataLayer.push(window.digiCustom.generateDigiClickCategoryEventData(category, index, term));
};

window.digiCustom.handleHistory = function (history, index, term, correction) {
  var decodedHistoryName = window.Digi.utils.decode(history);

  window.digiCustom.sendAutocompleteClickEvent(
      decodedHistoryName,
      'history',
      null,
      term,
      correction,
  );

  if (decodedHistoryName) {
    window.location.href = 'https://www.sima-land.ru/search/?q=' + decodedHistoryName;
  }
};

digiCustom.handleClickProduct = function (product, index, term, correction) {
  window.dataLayer.push(window.digiCustom.generateDigiClickProductEventData(product, index, term));
  window.digiCustom.sendAutocompleteClickEvent(
      product.name,
      'products',
      product.id,
      term,
      correction,
  );
};
