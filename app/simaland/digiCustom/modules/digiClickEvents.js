window.digiCustom.generateDigiClickQueryEventData = function (query, index) {
  return window.digiCustom.getPromoClickEvent(
      window.digiCustom.getGAEventFrame(
          'AnyqueryWidget',
          'searchHintClick',
          query,
      ),
      {
        'name': query,
        'creative': 'Anyquery: searchHint',
        'position': index + 1,
      },
  );
};

window.digiCustom.pushDigiClickTapEventData = function (tap, index) {
  window.dataLayer.push(
      window.digiCustom.getPromoClickEvent(
          window.digiCustom.getGAEventFrame(
              'AnyqueryWidget',
              'tapHintClick',
              tap,
          ),
          {
            'name': tap,
            'creative': 'Anyquery: tapHint',
            'position': index + 1,
          },
      ));
};


window.digiCustom.generateDigiClickCategoryEventData = function (category, index) {
  return window.digiCustom.getPromoClickEvent(
      window.digiCustom.getGAEventFrame(
          'AnyqueryWidget',
          'categoryHintClick',
          category.name,
      ),
      {
        'id': category.id,
        'name': category.name,
        'creative': 'Anyquery: categoryHint',
        'position': index + 1,
      },
  );
};

window.digiCustom.generateDigiClickProductEventData = function (product, index, term) {
  var eventContent = {
    'name': product.name,
    'id': product.id,
    'price': product.price,
    'list': 'Anyquery: productHint',
    'position': index + 1,
  };

  if (product.brand) {
    eventContent.brand = product.brand;
  }

  return window.digiCustom.getClickEventContent(eventContent, term);
};
