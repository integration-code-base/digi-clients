window.digiCustom.replaceOpenIsButton = function () {
  if (window.digiCustom.isClientMobile()) {

    var openIsButton = document.querySelector('.header__nav-item[aria-label="Перейти в каталог"]');
    var openIsButtonContainer = openIsButton.parentElement;
    var openIsButtonCopy = openIsButton.cloneNode(true);

    openIsButton.remove();
    openIsButtonContainer.prepend(openIsButtonCopy);
  }
};

window.digiCustom.storeClientInputValue = function () {
  var clientInputDesktop = document.querySelector('.search-field__input');
  var clientInputMobile = document.querySelector('#search-header input');

  if (clientInputDesktop) {
    storeValue(clientInputDesktop);
  } else {
    storeValue(clientInputMobile);
  }

  function storeValue(clientInput) {
    if (clientInput.value) {
      window.digiCustom.storedClientInput = clientInput.value;
      clientInput.placeholder = clientInput.value;
      clientInput.value = '';
    }
  }
};

window.digiCustom.storeDigiInputValue = function () {
  var clientInputMobile = document.querySelector('#search-header input');

  clientInputMobile.placeholder = window.digiCustom.data.getDigiSearchInput().value;
  clientInputMobile.value = '';
};

window.digiCustom.setStoredInputValueDesktop = function () {
  if (!this.state.isMobile) {
    if (window.digiCustom.data.getDigiSearchInput().placeholder !== 'Поиск') {
      window.digiCustom.data.getDigiSearchInput().value = window.digiCustom.data.getDigiSearchInput().placeholder;
      this.state.term = window.digiCustom.data.getDigiSearchInput().value;
      window.digiCustom.data.getDigiSearchInput().placeholder = 'Поиск';
    }
  }
};

window.digiCustom.setStoredInputValueMobile = function () {
  if (this.state.isMobile) {
    var clientInputMobile = document.querySelector('#search-header input');

    if (clientInputMobile.placeholder !== 'Найдите нужное') {
      window.digiCustom.data.getDigiSearchInput().value = clientInputMobile.placeholder;
      this.state.term = window.digiCustom.data.getDigiSearchInput().value;
    }
  }
};

window.digiCustom.replaceSearchField = function () {
  var clientSearchWrapper = document.querySelector('.search-field__input-wrap');
  var digiSearchWrapper = document.querySelector('.digi-search');
  var clientSearchInput = clientSearchWrapper.querySelector('.search-field__input');
  var clientSearchButton = clientSearchWrapper.querySelector('.search-field__search-button');
  var clientTermClearButton = clientSearchWrapper.querySelector('.search-field__clear-button');
  var wasFocused = document.activeElement === window.digiCustom.data.getDigiSearchInput();

  clientSearchWrapper.append(digiSearchWrapper);
  if (window.digiCustom.storedClientInput) {
    window.digiCustom.data.getDigiSearchInput().placeholder = window.digiCustom.storedClientInput;
  }

  clientSearchInput.style.display = 'none';
  clientSearchButton.style.display = 'none';
  if (wasFocused) {
    window.digiCustom.data.getDigiSearchInput().focus();
  }
  if (clientTermClearButton) {
    clientTermClearButton.style.display = 'none';
  }
};

window.digiCustom.changeClientsStyles = function () {
  /* Так как на клиенте идет тест было решено вынести изменение стилей элементов клиента в скрипты */
  let stylesElement = document.createElement('style');
  let stylesText = '.popup__popup.search-tips__popup {display: none !important;}';

  stylesElement.innerHTML = stylesText;
  document.body.append(stylesElement);
};

window.digiCustom.setPosition = function () {
  if (!this.state.isMobile) {
    var inputBounding = document.querySelector('.digi-search-form__input')
        .getBoundingClientRect();
    var widget = document.querySelector('#digi-shield');
    var pageHeaderBounding = document.querySelector('#page-header').getBoundingClientRect();
    var ac = document.querySelector('.digi-ac');
    var mainScrollWrapper = document.querySelector('.digi-main-scroll-wrapper');

    widget.style.top = pageHeaderBounding.bottom + 'px';
    mainScrollWrapper.style.minHeight = document.documentElement.clientHeight - pageHeaderBounding.bottom + 'px';

    function calculateACPosition() {
      return -(pageHeaderBounding.bottom - inputBounding.bottom) + 10;
    }

    if (ac) {
      ac.style.top = calculateACPosition() + 'px';
    }
  }
};

window.digiCustom.defineZeroSearch = function () {
  var savedContext = this;

  if (document.location.search.match(/q=/) === null && !digiCustom.storedClientInput) {
    return;
  }

  if (!window.digiCustom.isClientMobile()) {
    window.digiCustom.WaitForElement('img[alt="Нет подходящих товаров"]').then(function (el) {
      var desktopZeroSign = document.querySelector('img[alt="Нет подходящих товаров"]');

      if (desktopZeroSign) {
        window.digiCustom.initZeroSearch.call(savedContext);
      }
    });
  }

  if (window.digiCustom.isClientMobile()) {
    window.digiCustom.WaitForElement('#category-page').then(function () {
      var mainContainer = document.querySelector('#category-page');
      var mobileSign = !!mainContainer &&
          mainContainer.innerText.match(/ничего не нашлось/gi) !== null;

      if (mobileSign) {
        window.digiCustom.initZeroSearch.call(savedContext);
      }
    });
  }
};

window.digiCustom.initZeroSearch = function () {
  window.digiCustom.data.isZeroSearch = true;

  this.state.isOpen = true;
  this.state.autocompleteData.products = [];
  this.state.term = digiCustom.storedClientInput;
  document.body.classList.add('digi-disable-scroll');
  window.scrollTo(0, 0);

  /* Ждем инита IS*/
  setTimeout(function () {
    var initButton = document.querySelector('.digi-init-search');

    initButton.click();
  }, 200);
};

window.digiCustom.closeByClickOutside = function (event) {
  if (!window.digiCustom.data.digiShield) {
    window.digiCustom.data.digiShield = document.querySelector('#digi-shield');
  }

  if (
      !event.target.closest(
          window.digiCustom.data.closeByClickOutsideIgnoreSelectorList.join(','),
      )
  ) {

    var overlay = document.querySelector('.digi-overlay');
    var ac = document.querySelector('.digi-ac');

    if (ac && overlay) {
      overlay.classList.add('digi-hidden');
      ac.classList.add('digi-hidden');
    }

    if (this.state.term.trim() !== this.state.termTemp.trim()) {
      this.state.term = this.state.termTemp;
    }

    if (!window.digiCustom.data.digiShield.classList.contains('digi-hidden') &&
        !window.digiCustom.data.isZeroSearch) {
      window.digiCustom.data.digiShield.classList.add('digi-hidden');
    }

    if (!window.digiCustom.data.isZeroSearch) {
      document.body.classList.remove('digi-disable-scroll');
    }
  }
};

