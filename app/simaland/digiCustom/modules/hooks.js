window.digiCustom.beforeAppCreate = function () {
  window.digiCustom.storedClientInput = '';

  window.digiCustom.replaceOpenIsButton();

  window.digiCustom.storeClientInputValue();
};

window.digiCustom.onAppCreate = function () {
  window.digiCustom.initData();

  /*
    200ms нужны для того, что бы элементы, с которыми происходят манипуляции
     внутри функции успели проинициализироваться
  */
  if (!window.digiCustom.isClientMobile()) {
    setTimeout(function () {
      window.digiCustom.replaceSearchField();
    }, 200);
  }

  window.digiCustom.defineZeroSearch.call(this);

  window.digiCustom.changeClientsStyles();
  var savedContext = this;

  if (!this.state.isMobile) {
    document.body.addEventListener('mousedown', function (event) {
      window.digiCustom.closeByClickOutside.call(savedContext, event);
    }, true);
  }
};

window.digiCustom.onContainerOpen = function () {
  /* Сетим только если виджет был закрыт, иначе при любом фокусе возвращает значение, по которому
был запрос, в том числе при нажатие кнопки стереть запрос */
  window.digiCustom.setStoredInputValueMobile.call(this);


  if (this.state.isMobile && !this.state.isOpen) {
    setTimeout(function () {
      document.querySelector(window.Digi.config.anyQuery.instantSearch.inputSelector).focus();
    }, 100);
  }
};

window.digiCustom.onSearchFieldFocus = function () {
  var savedContext = this;

  if (!window.digiCustom.data.digiShield) {
    window.digiCustom.data.digiShield = document.querySelector('#digi-shield');
  }

  if (!this.state.isOpen) {
    this.state.isOpen = true;
    document.body.classList.add('digi-disable-scroll');
  }

  window.digiCustom.setStoredInputValueDesktop.call(this);

  /* Если ac был скрыт с помощью digiCustom.closeByClickOutside */
  var ac = document.querySelector('.digi-ac');
  if (ac && ac.classList.contains('digi-hidden')) {
    ac.classList.remove('digi-hidden');
  }

  var overlay = document.querySelector('.digi-overlay');
  if (overlay && overlay.classList.contains('digi-hidden')) {
    overlay.classList.remove('digi-hidden');
  }

  if (!document.body.classList.contains('digi-disable-scroll')) {
    document.body.classList.add('digi-disable-scroll');
  }
  if (window.digiCustom.data.digiShield.classList.contains('digi-hidden')) {
    window.digiCustom.data.digiShield.classList.remove('digi-hidden');
  }


  /* just waiting for render */
  setTimeout(function () {
    window.digiCustom.setPosition.call(savedContext);
  }, 100);
};

window.digiCustom.onContainerClose = function () {
  let defaultCloseSearch = document.querySelector('[aria-label="Закрыть поиск"]');
  if (defaultCloseSearch) {
    defaultCloseSearch.click();
  }

  window.digiCustom.storeDigiInputValue();
};

window.digiCustom.onSearch = function () {
  var savedContext = this;

  /* Вынуждены обнулять из-за поведение:
   "При закрытии на мобилке с стертым запросом запрос остается" */
  if (this.state.isMobile) {
    this.state.termTemp = '';
  }

  setTimeout(function () {
    window.digiCustom.setPosition.call(savedContext);
  }, 100);

  window.digiCustom.pushZeroResultsViewEvent(this.state.term, this.state.data.products);
};
