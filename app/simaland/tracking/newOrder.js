// cartItems
(function() {
  var result = [];
  if (!dataLayer.filter(function(x) { return x.ecommerce }).length) return;
  var items = dataLayer.filter(function(x) { return x.ecommerce })[0].ecommerce.purchase.products;
  for (var i = 0; i < items.length; i++) {
    var item = {};
    item.productName = items[i].name;
    item.productId = items[i].id+'';
    item.price = items[i].price;
    item.salePrice = item.price;
    item.quantity = items[i].quantity;
    item.subtotal = Math.ceil(item.salePrice * item.quantity);
    result.push(item);
  }
  return result;
})();

// subtotal

(function () {
  var collectedDataName = 'subtotal';
  window.tryCounter = window.tryCounter || 1;

  function whatAreWeWaiting() {
    return dataLayer.filter(function (x) {
      return x.ecommerce;
    }).length;
  }

  if (!whatAreWeWaiting()) {
    if (window.tryCounter < 7) {
      window.tryCounter++;
      throw new Error();
    } else {
      delete window.tryCounter;
      throw new Error('Could not collect ' + collectedDataName);
    }
  } else {
    function collectData() {
      return dataLayer.filter(function(x) { return x.ecommerce })[0].ecommerce.purchase.actionField.revenue
    }

    return collectData();
  }
})();


// total
if (dataLayer.filter(function(x) { return x.ecommerce }).length) {
  dataLayer.filter(function(x) { return x.ecommerce })[0].ecommerce.purchase.actionField.revenue;
}
