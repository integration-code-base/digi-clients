// breadcrumbs
(function() {
  var result = [],
      items = document.querySelectorAll('.breadcrumbs .link');
  if (items.length > 0) {
    for (var i = 0; i < items.length; i++) {
      var item = items[i].innerText.trim();
      if (item !== 'Каталог') {
        result.push(item);
      }
    }
    result.push(document.querySelector('.breadcrumbs li:last-child span').innerText.trim())
  } else {
    result.push(document.querySelector('.likeH1').innerText.trim());
  }
  return result;
})();

// categoryId

if (document.querySelector('[data-category-id]') !== null) {
  document.querySelector('[data-category-id]').dataset.categoryId
}

//pageNumber

(function() {
  var pagination = document.querySelector('.b-pagination__page_space b');
  if (pagination === null) {
    pagination = document.querySelector('.b-pagination__page_selected');
  }

  if (pagination !== null) {
    return pagination.textContent;
  } else {
    return '1';
  }
})();

// pageProducts

(function() {
  var result = [];
  if (!dataLayer.filter(function(x) { return x.ecommerce }).length) return;
  var items = dataLayer.filter(function(x) { return x.ecommerce }).slice(-1)[0].ecommerce.impressions;
  for (var i = 0; i < items.length; i++) {
    var item = '';
    item= items[i].id+'';
    result.push(item);
  }
  return result;
})();