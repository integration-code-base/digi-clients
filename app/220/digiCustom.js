window.digiCustom = {
  init() {
    /* Функция вызывается в хуке открытия на десктопе, служит она для небольшой оптимизации, что бы сохранять ссылки
    * на элементы к которым часто идет обращение  */
    window.digiCustom.headerHeight = document.querySelector('.header-wrapper.header-main').getBoundingClientRect().height;
    window.digiCustom.toolsBarWrapHeight = document.querySelector('.toolsbar-wrap').getBoundingClientRect().height;
    window.digiCustom.searchPanel = document.querySelector('.page-container > .panel');
    window.digiCustom.searchPanelHeight = document.querySelector('.page-container > .panel').getBoundingClientRect().height
    window.digiCustom.digiShield = document.querySelector('#digi-shield');
    window.digiCustom.body = document.querySelector('body');
  },
  changeClientsStyles() {
    /* Так как на клиенте идет тест было решено вынести изменение стилей элементов клиента в скрипты
    * данный скрипт вызывается в хуке onAppCreate и отрабатывает только в том случае, если у нас включен IS */
    let stylesElement = document.createElement('style')
    let stylesText = `
        #main-search-form .ui-autocomplete {
            display: none !important;
        }
        .digi-disable-scroll .menu-catalog .js-menu {
            display: none !important;
        }
        .digi-disable-scroll .menu-catalog .js-menu.not-collapse {
            display: block !important;
        }
        .digi-disable-scroll .page-container > .panel {
            z-index: 1007 !important;
        }
        #search-input {
            font-size: 16px !important;
        }`;

    stylesElement.innerHTML = stylesText;
    document.body.append(stylesElement);
  },
  onSearch() {
    /* Функция для хука onSearch, отрабаотывает только на десктопе*/
    var scrollWrapper = document.querySelector('.digi-main-scroll-wrapper');

    if (!window.digiCustom.body.classList.contains('digi-isOpened')) {
      window.digiCustom.body.classList.add('digi-isOpened')
    }

    window.digiCustom.addHandleScroll(scrollWrapper);
    window.digiCustom.setDependingScrollBar(scrollWrapper);
    window.digiCustom.customScrollTop();
  },
  addHandleScroll(scrollWrapper) {
    scrollWrapper.addEventListener('scroll', digiCustom.handleScroll)
  },
  handleScroll() {
    var scrollWrapper = document.querySelector('.digi-main-scroll-wrapper');
    var currentScroll = scrollWrapper.scrollTop;

    /* Копируем в переменные элементы и их размеры */
    var headerHeight = window.digiCustom.headerHeight,
      toolsBarWrapHeight = window.digiCustom.toolsBarWrapHeight,
      searchPanel = window.digiCustom.searchPanel,
      searchPanelHeight = window.digiCustom.searchPanelHeight,
      digiShield = window.digiCustom.digiShield;

    /* В данном условия мы вычитаем из высота хедера, который у нас виден в нулевом положении, высоту,
    которая должна остаться после того, как проскролим. Так же пока действительно данное условие мы изменяем выосоту и положение
      нашего элемента и так же скроллим документ */
    if (currentScroll <= headerHeight - toolsBarWrapHeight) {
      /* Так как мы внутри данной функции так же меняем высоту элемента нам нужно сохранить положение скролла
      перед выполнение данных действий. Если нужно будет рефакторить или что то менять, то нужно будет обращать внимание
       на те моменты, когда у нас высота элемента вместе с сколом небольшая */
      var savedCurrentScroll = currentScroll;
      document.documentElement.scroll(0, currentScroll)
      digiShield.style.top = Math.ceil(headerHeight + searchPanelHeight - currentScroll - 10) + 'px'; /* ten it's a digishield margin */
      /* на самом деле здесь легко запутаться. Если по простому то высота документа минус сумма двух элементов это начальное положение элемента
      * и мы из него так же вычетаем текущую прокрутку*/
      digiShield.style.height = Math.ceil(document.documentElement.clientHeight - (headerHeight + searchPanelHeight - currentScroll - 10)) + 'px';
      /* В конце мы корректируем значение скрола, так как изменилось высота */
      scrollWrapper.scroll(0, savedCurrentScroll)
    } else {
      /* Не очень оптимальное действие, но на данном этапе мы просто закрпляем позиции и высоты элементов в пололжении, когда мы проскролили
      * и скрыт хедер. Веротянее всего без этого были баги. Конечно хорошо было бы это оптимизировать и добавить внутрь проверки,
      *  что положение зафиксировано  */
      document.documentElement.scroll(0, headerHeight - toolsBarWrapHeight)
      digiShield.style.top = Math.ceil(searchPanelHeight + toolsBarWrapHeight - 10) + 'px';
      digiShield.style.height = Math.ceil(document.documentElement.clientHeight - searchPanelHeight - toolsBarWrapHeight + 10) + 'px';
    }

    /* Добовляем класс на элемент обретки строки, которая добавляет затемнение (это клиентское поведение). Так же добавляем наш класс
    * который меняет отображение автокомплита, на позициориновие не под строкой поиска, а под обреткой,
    *  так же скрываются заглушки для скрытия скругления  */
    if (currentScroll > headerHeight - toolsBarWrapHeight) {
      searchPanel.classList.add('js-sticky', 'pre-fix', 'scroll-to-fixed-fixed')
      window.digiCustom.body.classList.add('digi-is-container-scrolled')
    } else {
      window.digiCustom.body.classList.remove('digi-is-container-scrolled');
      searchPanel.classList.remove('js-sticky', 'pre-fix', 'scroll-to-fixed-fixed');
    }
  },
  setDependingScrollBar(scrollWrapper) {
    /* Функция, которая дает левый отступ и позиционирование бэкграунда элементу обертки
    * расчитывается оно исходя из ширины скролла, который может отличаться в зависемости от браузера */
    var scrollbarWidth = scrollWrapper.offsetWidth - scrollWrapper.clientWidth;
    scrollWrapper.style.marginLeft = scrollbarWidth + 'px';
    /* 235 это константная позиция по оси Y */
    scrollWrapper.style.backgroundPosition = '-' + scrollbarWidth + 'px -235px';
  },
  closeSearch() {
    if (window.digiCustom.body.classList.contains('digi-isOpened')) {
      window.digiCustom.body.classList.remove('digi-isOpened')
    }
  },
  mobileCommonToggle: function (e) {
    var target = e.target;

    while (!target.classList.contains('digi-facet_common') && target) {
      target = target.parentElement;
    }
    var options = target.querySelector('.digi-facet-options');
    var toggleIcon = target.querySelector('.digi-facet__toggle__icon');

    if (options) {
      if (!options.classList.contains('digi-facet-options-opened')) {
        options.classList.add('digi-facet-options-opened');
        toggleIcon.classList.add('digi-facet__toggle__icon-active');
      } else {
        options.classList.remove('digi-facet-options-opened');
        toggleIcon.classList.remove('digi-facet__toggle__icon-active');
      }
    }
  },
  mobileSliderToggle: function (e) {
    var target = e.target;

    while (!target.classList.contains('digi-facet_slider') && target) {
      target = target.parentElement;
    }

    var slider = target.querySelector('.vue-slider');
    var toggleIcon = target.querySelector('.digi-facet__toggle__icon');

    if (!slider.classList.contains('vue-slider-opened')) {
      slider.classList.add('vue-slider-opened');
      toggleIcon.classList.add('digi-facet__toggle__icon-active');
    } else {
      slider.classList.remove('vue-slider-opened');
      toggleIcon.classList.remove('digi-facet__toggle__icon-active');
    }
  },
  addToCartDesktop(productCode, quantity = 1) {
    addToCart(productCode, quantity)
  },
  addToCompare(productCode, target) {
    target.closest('.digi-product__compare').classList.add('digi-product__compare-active');
    window.modalAJAH(null, {url: '/compare/help/' + productCode + '/', containerId: 'compareModal', width: 500});

    if (window.compareProduct) {
      window.compareProduct(productCode, 0, 1);
    }
  },
  getAvailability(productCode, productLink, e) {
    /* Функция рендера элемента доступности */
    const availableButton = e.target.closest('.digi-product__available-button');
    const availableButtonWrapper = e.target.closest('.digi-product__available-wrapper-desktop');

    if (availableButton.classList.contains('digi-product__available-button-active')) {
      return
    }

    const availableListWrapper = `
        <div class="digi-product__available-list-wrapper">
          <div class="digi-product__available__arrow"></div>
          <div class="digi-product__available-preloader">Загрузка...</div>
        </div>
      `;

    availableButton.classList.add('digi-product__available-button-active');

    availableButtonWrapper.insertAdjacentHTML('afterbegin', availableListWrapper)
    const listWrapper = availableButtonWrapper.querySelector('.digi-product__available-list-wrapper');

    setRelativePosition()

    function setRelativePosition() {
      const availableButtonWrapperCoordinate = availableButtonWrapper.getBoundingClientRect();
      const productsWrapperCoordinate = availableButton.closest('.digi-products-grid').getBoundingClientRect();
      const availableWrapperWidth = 600;
      const offsetFromCenter = availableButtonWrapperCoordinate.width / 2 - availableWrapperWidth / 2;
      const availableButtonCoordinate = availableButton.getBoundingClientRect();
      const arrow = availableButtonWrapper.querySelector('.digi-product__available__arrow');

      if (availableButtonWrapperCoordinate.left + offsetFromCenter < productsWrapperCoordinate.left) {
        listWrapper.style.left = 0
        arrow.style.left = availableButtonWrapperCoordinate.width - availableButtonCoordinate.width / 2 + 'px';
      } else if (availableButtonWrapperCoordinate.left + 600 + offsetFromCenter > productsWrapperCoordinate.right) {
        listWrapper.style.right = 0
        arrow.style.right = availableButtonCoordinate.width / 2 + 'px';
      } else {
        listWrapper.style.left = offsetFromCenter + 'px'
        arrow.style.left = availableWrapperWidth / 2 + availableButtonWrapperCoordinate.width / 2 - availableButtonCoordinate.width / 2 + 'px';
      }
    }

    window.digiCustom.requestAvailability(productCode).then((response) => {
        if (availableButton.classList.contains('digi-product__available-button-active')) {
          renderAvailabilityScreen(response)
        }
      }
    );

    function renderAvailabilityScreen(response) {
      let options = JSON.parse(response);
      let listing = availableButtonWrapper.querySelector('.digi-product__available-list-wrapper');
      let preloader = availableButtonWrapper.querySelector('.digi-product__available-preloader');

      if (!options.absent) {
        let availableList = `
            <div class="digi-product__available-list">
              ${renderDeliveriesWrap(options.delivery.pickup, 'pickup')}
              ${renderDeliveriesWrap(options.delivery.delivery, 'delivery')}
            </div>
          `;

        function renderDeliveriesWrap(options, type) {
          if (Object.keys(options).length) {
            return `
                <div class="digi-product__deliveries-wrap">
                  <div class="digi-product__deliveries-head">${options.title}</div>
                  <ul class="digi-product__deliveries-list">
                    ${getDeliveriesList(options.items, type)}
                  </ul>
  
                  <div class="digi-product__deliveries__all-offers">
                     <a href="${productLink}">${type === 'pickup' ? `Все магазины (${options['total_count']})` : 'Все варианты доставки'}</a>
                  </div>
  
                </div>
              `;
          } else {
            return ''
          }
        }

        function getDeliveriesList(items, type) {
          let deliveryList = ''

          items.forEach((item) => {
            deliveryList += DeliveriesItem(item, type);
          })

          return deliveryList;
        }

        function DeliveriesItem(itemOptions, type) {
          return `
                <li class="digi-product__deliveries-list__item">
                  <div class="digi-product__deliveries__delivery-title">${(type === 'delivery') ? itemOptions['full_title'] : itemOptions['address']}</div>
                  <div class="digi-product__deliveries__delivery-delay">
                    <strong>${itemOptions.time.text[0]}</strong>, ${itemOptions.time.text[1]}
                  </div>
                  <div class="digi-product__deliveries__delivery-price">${itemOptions.price ? `${itemOptions.price} р.` : ''}</div>
                  <div class="digi-product__button digi-product__deliveries__delivery-buy" data-id="${productCode}" onclick="window.digiCustom.addToCartDesktop(event.target.dataset.id)">В корзину</div>
                </li>
              `;
        }

        listing.insertAdjacentHTML('afterbegin', availableList)
        preloader.remove();
      } else {
        let absentHTML = `
            <div class="digi-product__available-absent">Нет в наличии</div>
          `
        listing.insertAdjacentHTML('afterbegin', absentHTML)
        preloader.remove();
      }
    }

    setTimeout(() => {
      window.addEventListener('click', closeOutsideElement)

    }, 1)

    function closeOutsideElement(e) {
      if (!e.target.closest('.digi-product__available-list-wrapper')) {

        listWrapper.remove();
        availableButton.classList.remove('digi-product__available-button-active');
        window.removeEventListener('click', closeOutsideElement);
      }
    }
  },
  requestAvailability(productCode) {
    /* Запрос к API клиента */
    return new Promise((resolve, reject) => {
      const XHR = new XMLHttpRequest();
      let url = '/catalog/list/delivery/options/';
      let body = 'code=' + productCode + '&add_list_size=1';

      XHR.open('POST', url);

      XHR.withCredentials = true;
      XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      XHR.setRequestHeader('Accept', 'application/json, text/javascript, */*; q=0.01');

      XHR.onload = () => {
        if (XHR.status >= 400) {
          reject(XHR.response)
        } else {
          resolve(XHR.response)
        }
      }

      XHR.onerror = () => {
        reject(XHR.response)
      }

      XHR.send(body);
    })
  },
  customScrollTop() {
    /* Срабатывает на хук фокуса строки поиска на десктопе, скролит страницу в изначальное положение */
    if (!window.digiCustom.body.classList.contains('digi-isOpened')) {
      var headerHeight = window.digiCustom.headerHeight,
        toolsBarWrapHeight = window.digiCustom.toolsBarWrapHeight,
        searchPanelHeight = window.digiCustom.searchPanelHeight,
        digiShield = window.digiCustom.digiShield;

      document.documentElement.scroll(0, 0)
      digiShield.style.top = headerHeight + searchPanelHeight - 10 + 'px'
      digiShield.style.height = document.documentElement.clientHeight - searchPanelHeight - toolsBarWrapHeight + 10 + 'px';
    }
  },
  getOptions: function (currentOptionType) {
    /* Убираем из селекта текущую опцию с помощью фильтра */
    var options = [{'label': 'Сначала дешевле', 'type': 'PRICE_ASC'}, {
      'label': 'Сначала дороже',
      'type': 'PRICE_DESC'
    }, {'label': 'По популярности', 'type': 'DEFAULT'}];
    return options.filter(function (it) {
      return it.type !== currentOptionType
    });
  },
  splitLead: function (leadStr) {
    if (leadStr.match(/  /g)) {
      return leadStr.split('  ').slice(1).join('  ');
    }
    return leadStr;
  },
  setFacetName: function (name, unit) {
    if (unit && name.slice(-1) !== ')') {
      return name + ' (' + unit + ')';
    }
    return name
  },
  setFacetNameCurrentOpen: function (currentOpen, facetList) {
    /* Через функцию файн во всех фасетах ищем текущий открытый и вызывем функцию
    * получение имени с его аргументами*/
    var currentFacet = facetList.find(it => it.name === currentOpen);
   
    return window.digiCustom.setFacetName(currentFacet.name, currentFacet.unit);
  }
}