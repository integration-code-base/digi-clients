{
  "silent": true,
  "hooks": {
  "onAppCreate": "(function() { digiCustom.changeClientsStyles() })",
    "onContainerOpen": "(function () { if (this.state.isMobile) { setTimeout(function () { document.querySelector('.digi-search-form__input-block input').focus()}, 100)}; this.state.hiddenByScroll = false; if (!this.state.isMobile){digiCustom.init()} })",
    "onContainerClose": "(function() { if (!this.state.isMobile){digiCustom.closeSearch()}  if(this.state.isMobile) { document.querySelector('#main-search-form').parentElement.style.display = 'none' } })",
    "onSearchFieldFocus": "(function() { if (!this.state.isMobile){digiCustom.customScrollTop()} })",
    "onSearchFieldInput": "(function() {})",
    "onBeforeSearch": "(function() { this.state.sort = 'DEFAULT'; this.state.hiddenByScroll = false;  })",
    "onSearch": "(function() { if (!this.state.isMobile){digiCustom.onSearch()} })",
    "onSearchButtonClick": "(function() {})",
    "onSearchFormSubmit": "(function() {})",
    "onRefinementClick": "(function() {})",
    "onAutocompleteClickTap": "(function(payload) {})",
    "onAutocompleteClickQuery": "(function(payload) {})",
    "onAutocompleteClickHistory": "(function(payload) {})",
    "onAutocompleteClickCategory": "(function(payload) {})",
    "onAutocompleteClickProduct": "(function(payload) {})",
    "onFacetChange": "(function(payload) {})",
    "onCategoryClick": "(function(payload) {})",
    "onInitViaUrlParams": "(function(payload) { if (!this.state.isMobile){digiCustom.onSearch()} })"
}
}