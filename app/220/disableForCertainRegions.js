var currentRegion = getCurrentRegion();
var allowedRegions = {
  '16': '16',
  '23': '23',
  '50': '50',
  '52': '52',
  '54': '54',
  '61': '61',
  '63': '63',
  '66': '66',
  '71': '71',
  '77': '77',
  '78': '78',
}

function getCurrentRegion() {
  if (document.querySelector('[data-city]') !== null && document.querySelector('[data-city]').dataset.city !== null) {
    return document.querySelector('[data-city]').dataset.city.slice(0, 2);
  }
}

if (!allowedRegions[currentRegion]) {
  Config.anyQuery.instantSearch.enabled = false;
  Config.anyQuery.instantSearch.mobileEnabled = false;
}